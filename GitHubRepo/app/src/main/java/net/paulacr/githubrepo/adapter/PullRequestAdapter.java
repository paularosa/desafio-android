package net.paulacr.githubrepo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.paulacr.githubrepo.R;
import net.paulacr.githubrepo.model.pullrequest.PullRequest;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by paularosa on 3/26/16.
 */
public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private Context context;
    private List<PullRequest> pullRequests;

    public PullRequestAdapter(Context context, List<PullRequest> pullRequests) {
        this.context = context;
        this.pullRequests = pullRequests;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.prName)
        TextView prName;
        @Bind(R.id.prDescription)
        TextView prDescription;
        @Bind(R.id.prUsername)
        TextView prUsername;
        @Bind(R.id.prDate)
        TextView prDate;
        @Bind(R.id.prProfilePicture)
        CircleImageView prProfilePicture;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pullrequest, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = pullRequests.get(position);

        holder.prName.setText(pullRequest.getTitle());
        holder.prDescription.setText(pullRequest.getDescription());
        holder.prUsername.setText(pullRequest.getUser().getUsername());
        holder.prDate.setText(pullRequest.getDate());


        Picasso.with(context).load(pullRequest.getUser().getAvatarUrl())
                .placeholder(R.drawable.user_placeholder).into(holder.prProfilePicture);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

}
