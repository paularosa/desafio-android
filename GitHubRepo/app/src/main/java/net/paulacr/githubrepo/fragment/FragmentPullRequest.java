package net.paulacr.githubrepo.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.paulacr.githubrepo.R;
import net.paulacr.githubrepo.adapter.PullRequestAdapter;
import net.paulacr.githubrepo.interfaces.onListItemClick;
import net.paulacr.githubrepo.model.User;
import net.paulacr.githubrepo.model.pullrequest.PullRequest;
import net.paulacr.githubrepo.network.RestApi;
import net.paulacr.githubrepo.utils.DividerItemDecorator;
import net.paulacr.githubrepo.utils.NetworkConnectionChecker;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by paularosa on 3/27/16.
 */
public class FragmentPullRequest extends Fragment implements onListItemClick {

    @Bind(R.id.listPullRequest)
    RecyclerView listRepository;

    private ProgressDialog dialog;
    public static final String TAG = "fragmentRepository";
    private int page = 1;
    private List<PullRequest> itemsList;
    private List<User> userList;
    private PullRequestAdapter adapter;

    //Receiver
    private BroadcastReceiver receiver;
    private IntentFilter networkFilter;

    //Keys for Bundle args
    private static final String USER_KEY = "username";
    private static final String REPO_KEY = "repositoryName";

    //Bundle args Variables
    private String username;
    private String repositoryName;


    public static FragmentPullRequest newInstance(String username, String repositoryName) {

        FragmentPullRequest fragmentPullRequest = new FragmentPullRequest();

        Bundle args = new Bundle();
        args.putString(USER_KEY,username);
        args.putString(REPO_KEY,repositoryName);

        fragmentPullRequest.setArguments(args);
        return fragmentPullRequest;
    }

    //---------------------------------------------
    // LifeCycle
    //---------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_pullrequest, container, false);
        ButterKnife.bind(this, view);



        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        startLoading();
        setupRepositoryList();

        if(NetworkConnectionChecker.isConnected(getActivity())) {
            //call request
            performRepositoryRequest(username, repositoryName);
        } else {
            stopLoading();
            generateSnackBarErrorMessage(getResources().getString(R.string.error_network_connection),
                    getResources().getString(R.string.action_try_again));
        }


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupNetworkReceiver();

        username = getArguments() != null ? getArguments().getString(USER_KEY) : "";
        repositoryName = getArguments() != null ? getArguments().getString(REPO_KEY) : "";
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, networkFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    //---------------------------------------------
    // Setup Views
    //---------------------------------------------

    private void setupRepositoryList() {

        itemsList = new ArrayList<>();
        userList = new ArrayList<>();

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        adapter = new PullRequestAdapter(getActivity(), itemsList);

        listRepository.setLayoutManager(manager);
        listRepository.addItemDecoration(new DividerItemDecorator(getContext(), DividerItemDecorator.VERTICAL_LIST));
        listRepository.setAdapter(adapter);
    }

    private void startLoading() {
        if(dialog == null) {
            dialog = new ProgressDialog(getActivity());
        }
        dialog.setTitle("Please Wait");
        dialog.setMessage("Searching for repositories");
        dialog.show();
    }

    private void stopLoading() {
        dialog.dismiss();
    }

    //---------------------------------------------
    // Errors feedback
    //---------------------------------------------

    private void generateSnackBarErrorMessage(String message, String action) {
        Snackbar.make(listRepository, message, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(NetworkConnectionChecker.isConnected(getActivity())) {
                            startLoading();
                            //performRepositoryRequest();
                        } else {
                            generateSnackBarErrorMessage(getResources().getString(R.string.error_network_connection),
                                    getResources().getString(R.string.action_try_again));
                        }

                    }
                })
                .show();
    }

    //---------------------------------------------
    // Requests
    //---------------------------------------------

    private void performRepositoryRequest(String user, String repoName) {
        Call<List<PullRequest>> service = RestApi.getClient().getPullRequests(user, repoName);
        service.enqueue(new Callback<List<PullRequest>>() {


            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                itemsList.addAll(response.body());
                adapter.notifyItemRangeChanged(adapter.getItemCount(), itemsList.size() - 1);
                stopLoading();
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                stopLoading();
                generateSnackBarErrorMessage(getResources().getString(R.string.error_request_service),
                        getResources().getString(R.string.action_try_again));
            }
        });
    }


    //---------------------------------------------
    // Receiver
    //---------------------------------------------

    private void setupNetworkReceiver() {
        /**
         * This broadcast receiver is responsable to detect network connections changes
         * so, when users turn on the internet another request can be performed
         */
        networkFilter = new IntentFilter();
        networkFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(NetworkConnectionChecker.isConnected(context)) {
                    startLoading();
                    //performRepositoryRequest();
                }
            }
        };
    }

    @Override
    public void onItemClicked(int position) {

    }
}
