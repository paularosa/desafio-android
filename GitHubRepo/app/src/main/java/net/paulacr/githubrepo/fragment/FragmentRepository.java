package net.paulacr.githubrepo.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.paulacr.githubrepo.MainActivity;
import net.paulacr.githubrepo.R;
import net.paulacr.githubrepo.adapter.RepositoryAdapter;
import net.paulacr.githubrepo.interfaces.onListItemClick;
import net.paulacr.githubrepo.interfaces.onScrollMore;
import net.paulacr.githubrepo.listener.OnScrollMoreRecyclerview;
import net.paulacr.githubrepo.model.repository.Item;
import net.paulacr.githubrepo.model.repository.JavaRepository;
import net.paulacr.githubrepo.model.repository.Repository;
import net.paulacr.githubrepo.model.User;
import net.paulacr.githubrepo.network.RestApi;
import net.paulacr.githubrepo.utils.DividerItemDecorator;
import net.paulacr.githubrepo.utils.NetworkConnectionChecker;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by paularosa on 3/26/16.
 */
public class FragmentRepository extends Fragment implements onScrollMore , onListItemClick{

    @Bind(R.id.listRepo)
    RecyclerView listRepository;

    private ProgressDialog dialog;
    public static final String TAG = "fragmentRepository";
    private int page = 1;
    private List<Item> itemsList;
    private List<User> userList;
    private RepositoryAdapter adapter;

    private long repositoryItemSize = 0;
    private long totalCount;

    //Receiver
    private BroadcastReceiver receiver;
    private IntentFilter networkFilter;

    private onListItemClick listener;

    //Parameters for next Fragment - pull requests
    private String userName;
    private String repositoryName;


    public static FragmentRepository newInstance() {
        return new FragmentRepository();
    }

    //---------------------------------------------
    // LifeCycle
    //---------------------------------------------

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_repository, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        startLoading();
        setupRepositoryList();
        adapter.setListener(this);
        if(NetworkConnectionChecker.isConnected(getActivity())) {
            performRepositoryRequest("1");
        } else {
            stopLoading();
            generateSnackBarErrorMessage(getResources().getString(R.string.error_network_connection),
                    getResources().getString(R.string.action_try_again));
        }


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupNetworkReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, networkFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    //---------------------------------------------
    // Setup Views
    //---------------------------------------------

    private void setupRepositoryList() {

        itemsList = new ArrayList<>();
        userList = new ArrayList<>();

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        adapter = new RepositoryAdapter(getActivity(), itemsList, userList);
        listRepository.setLayoutManager(manager);

        OnScrollMoreRecyclerview endlessScroll = new OnScrollMoreRecyclerview(manager);
        endlessScroll.setListener(this);

        listRepository.addOnScrollListener(endlessScroll);
        listRepository.addItemDecoration(new DividerItemDecorator(getContext(), DividerItemDecorator.VERTICAL_LIST));
        listRepository.setAdapter(adapter);
    }

    private void startLoading() {
        if(dialog == null) {
            dialog = new ProgressDialog(getActivity());
        }
        dialog.setTitle("Please Wait");
        dialog.setMessage("Searching for repositories");
        dialog.show();
    }

    private void stopLoading() {
        dialog.dismiss();
    }

    //---------------------------------------------
    // Pages Control
    //---------------------------------------------
    @Override
    public void onLoadMorePages(int page) {
        if(hasMorePages(totalCount, repositoryItemSize)) {
            startLoading();
            setPage(page);
            performRepositoryRequest(String.valueOf(page));
        }

    }

    private void setPage(int page) {
        this.page = page;
    }

    private int getPage() {
        return page;
    }

    private boolean hasMorePages(long totalCount, long listSizeCurrentPage) {
        return listSizeCurrentPage != totalCount;
    }

    //---------------------------------------------
    // Errors feedback
    //---------------------------------------------

    private void generateSnackBarErrorMessage(String message, String action) {
        Snackbar.make(listRepository, message, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(NetworkConnectionChecker.isConnected(getActivity())) {
                            startLoading();
                            performRepositoryRequest(String.valueOf(getPage()));
                        } else {
                            generateSnackBarErrorMessage(getResources().getString(R.string.error_network_connection),
                                    getResources().getString(R.string.action_try_again));
                        }

                    }
                })
                .show();
    }

    //---------------------------------------------
    // Requests
    //---------------------------------------------

    private void performRepositoryRequest(String page) {
        Call<JavaRepository> service = RestApi.getClient().getRepository("language:java", "stars", page);
        service.enqueue(new Callback<JavaRepository>() {
            @Override
            public void onResponse(Call<JavaRepository> call, Response<JavaRepository> response) {
                try {
                    response.body().getItem();

                    Repository.getInstance().setJavaRepository(response.body());
                    List<Item> items = Repository.getInstance().getJavaRepository().getItem();

                    itemsList.addAll(items);
                    adapter.notifyItemRangeChanged(adapter.getItemCount(), itemsList.size() - 1);
                    totalCount = response.body().getTotalCount();
                    repositoryItemSize += items.size();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                stopLoading();
            }

            @Override
            public void onFailure(Call<JavaRepository> call, Throwable t) {
                stopLoading();
                generateSnackBarErrorMessage(getResources().getString(R.string.error_request_service),
                        getResources().getString(R.string.action_try_again));
            }
        });
    }

    //---------------------------------------------
    // Receiver
    //---------------------------------------------

    private void setupNetworkReceiver() {
        /**
         * This broadcast receiver is responsable to detect network connections changes
         * so, when users turn on the internet another request can be performed
         */
        networkFilter = new IntentFilter();
        networkFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(NetworkConnectionChecker.isConnected(context)) {
                    startLoading();
                    performRepositoryRequest(String.valueOf(getPage()));
                }
            }
        };
    }

    @Override
    public void onItemClicked(int position) {

        Item item = Repository.getInstance().getJavaRepository().getItem().get(position);

        String username = item.getOwner().getUserName();
        String repositoryName = item.getName();

        addFragmentPullRequest(FragmentPullRequest
                .newInstance(username, repositoryName), FragmentPullRequest.TAG);
    }

    private void addFragmentPullRequest(Fragment fragment, String TAG) {
        final FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, TAG).addToBackStack(TAG).commit();
    }
}
