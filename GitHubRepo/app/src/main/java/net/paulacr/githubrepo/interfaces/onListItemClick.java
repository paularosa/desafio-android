package net.paulacr.githubrepo.interfaces;

/**
 * Created by paularosa on 3/27/16.
 */
public interface onListItemClick {

    void onItemClicked(int position);
}
