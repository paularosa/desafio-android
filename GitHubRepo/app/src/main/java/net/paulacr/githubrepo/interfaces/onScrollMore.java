package net.paulacr.githubrepo.interfaces;

/**
 * Created by paularosa on 3/26/16.
 */
public interface onScrollMore {

    void onLoadMorePages(int page);
}
