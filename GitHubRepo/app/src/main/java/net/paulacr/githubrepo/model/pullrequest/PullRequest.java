package net.paulacr.githubrepo.model.pullrequest;

import com.google.gson.annotations.SerializedName;

import net.paulacr.githubrepo.model.User;

/**
 * Created by paularosa on 3/27/16.
 */
public class PullRequest {

    @SerializedName("id")
    private long id;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String description;

    @SerializedName("created_at")
    private String date;

    @SerializedName("user")
    private User_ user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User_ getUser() {
        return user;
    }

    public void setUser(User_ user) {
        this.user = user;
    }
}
