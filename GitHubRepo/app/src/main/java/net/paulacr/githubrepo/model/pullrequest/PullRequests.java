package net.paulacr.githubrepo.model.pullrequest;

import com.google.gson.annotations.SerializedName;

import net.paulacr.githubrepo.model.User;

import java.util.List;

/**
 * Created by paularosa on 3/27/16.
 */
public class PullRequests {

    private List<PullRequest> pullRequest;
    private User user;
    private static PullRequests instance;


    public static PullRequests getInstance() {
        if(instance == null) {
            instance = new PullRequests();
        }
        return instance;
    }

    public List<PullRequest> getPullRequest() {
        return pullRequest;
    }

    public void setPullRequest(List<PullRequest> pullRequest) {
        this.pullRequest = pullRequest;
    }
}
