package net.paulacr.githubrepo.model.pullrequest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 3/27/16.
 */
public class User_ {

    @SerializedName("id")
    private long id;
    @SerializedName("name")
    private String name;
    @SerializedName("login")
    private String username;
    @SerializedName("avatar_url")
    String avatarUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
