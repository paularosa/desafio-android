package net.paulacr.githubrepo.model.repository;

import com.google.gson.annotations.SerializedName;

/**
 * Created by paularosa on 3/26/16.
 */
public class Owner {

    @SerializedName("id")
    private long id;
    @SerializedName("login")
    private String userName;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("url")
    private String userUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }
}
