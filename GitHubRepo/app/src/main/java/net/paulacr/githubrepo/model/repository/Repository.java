package net.paulacr.githubrepo.model.repository;

import net.paulacr.githubrepo.model.User;

/**
 * Created by paularosa on 3/27/16.
 */
public class Repository {

    private JavaRepository javaRepository;
    private User user;
    private static Repository instance;

    public static Repository getInstance() {
        if(instance == null) {
            instance = new Repository();
        }
        return instance;
    }

    public JavaRepository getJavaRepository() {
        return javaRepository;
    }

    public void setJavaRepository(JavaRepository javaRepository) {
        this.javaRepository = javaRepository;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
