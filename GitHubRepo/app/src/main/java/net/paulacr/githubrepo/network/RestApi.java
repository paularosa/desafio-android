package net.paulacr.githubrepo.network;

import net.paulacr.githubrepo.model.pullrequest.PullRequest;
import net.paulacr.githubrepo.model.repository.JavaRepository;
import net.paulacr.githubrepo.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by paularosa on 3/26/16.
 */
public class RestApi {

    private static final String BASE_URL = "https://api.github.com/";
    private static final String ENDPOINT_REPOSITORY = "search/repositories";
    private static final String ENDPOINT_PULL_REQUEST = "repos/{user}/{repoName}/pulls";

    private static Retrofit retrofit;
    private static GitHubService gitHubService;

    public static GitHubService getClient() {
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            gitHubService = retrofit.create(GitHubService.class);
        }
        return gitHubService;
    }

    public interface GitHubService {
        @GET(ENDPOINT_REPOSITORY)
        Call<JavaRepository> getRepository(
                @Query("q") String language,
                @Query("sort") String sortType,
                @Query("page") String page);

        @GET(ENDPOINT_PULL_REQUEST)
        Call<List<PullRequest>> getPullRequests(
            @Path("user") String user,
            @Path("repoName") String repoName);

    }
}
